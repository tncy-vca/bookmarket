package net.tncy.vca.bookmarket.data;

public class Bookstore {
    public int id;
    public String name;
    public InventoryEntry[] inventoryEntries;

    public Bookstore(int id, String name, InventoryEntry[] inventoryEntries) {
        this.id = id;
        this.name = name;
        this.inventoryEntries = inventoryEntries;
    }
}
