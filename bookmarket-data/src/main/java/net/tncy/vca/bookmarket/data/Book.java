package net.tncy.vca.bookmarket.data;

import net.tncy.vca.validator.ISBN;

public class Book {
    public enum BookFormat {
        BROCHE, POCHE;
    }
    public int id;
    public String title;
    public String author;
    public String publisher;
    public BookFormat bookFormat;

    @ISBN
    public String isbn;

    public Book(int id, String title, String author, String publisher, BookFormat bookFormat, String isbn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.bookFormat = bookFormat;
        this.isbn = isbn;
    }
}
